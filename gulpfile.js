'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

//** CONFIG **//
var sourceFolder = './source';
//Stylesheets
var destinationCss = './public/assets/css';
var filesScss = sourceFolder + '/scss/**/*.scss';

//Javascripts
var destinationJs = './public/assets/js';
var filesJs = sourceFolder + '/javascripts/';

//Server and Html files
var serverUrl = 'forsikring.18digital.dev';
var templatePath = './craft/templates/**/*';

// ** CONFIG END //


var useJsFiles = [
    filesJs + 'jquery.min.js',
    filesJs + 'jquery.easing.1.3.js',
    filesJs + 'bootstrap.min.js',
    filesJs + 'site.js',
    filesJs + 'masonry.pkgd.min.js',
    filesJs + 'script.js'
];

gulp.task('sass', function () {
    return gulp.src(filesScss)

        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
        .pipe(gulp.dest(destinationCss))
        .pipe(browserSync.reload({stream: true}));

});

gulp.task('templates', function () {
    return gulp.src('./source/templates/**/*')

        .pipe(gulp.dest('./craft/templates'))


});

gulp.task('scripts', function () {
    return gulp.src(useJsFiles)
        .pipe(concat('all.js'))
        .pipe(gulp.dest(destinationJs));
});

gulp.task('serve', ['sass'], function () {

    browserSync.init({
        proxy: serverUrl,
        ws: true

    });
    gulp.watch(filesScss, ['sass']);
    gulp.watch(filesJs, ['scripts']);
    gulp.watch('./source/templates/**/*',['templates']).on('change', browserSync.reload);

});
