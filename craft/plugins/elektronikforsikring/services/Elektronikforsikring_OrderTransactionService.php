<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;


class Elektronikforsikring_OrderTransactionService extends BaseApplicationComponent
{
    public function getTransactionDataWithOrderIdSimple($_orderId)
    {
        $transactionData = craft()->db->createCommand()
            ->from('elektronikforsikring_orderattributes')
            ->where('orderId=' . $_orderId)
            ->queryAll();

        return $transactionData;


    }

    public function getAllTransactionData()
    {
        $transactionData = craft()->db->createCommand()
            ->from('elektronikforsikring_orderattributes')->select('')->queryAll();

        return $transactionData;

    }

    public function getTransactionDataWithOrderId($_id)
    {
        $query = craft()->db->createCommand()
            ->select('craft_commerce_orders.*')
            ->addSelect('craft_elektronikforsikring_orderattributes.*')
            ->from('commerce_orders')
            ->join('elektronikforsikring_orderattributes', 'craft_commerce_orders.id = craft_elektronikforsikring_orderattributes.orderId')
            ->where('craft_commerce_orders.id=' . $_id)
            ->queryAll();

        return $query;

    }

    public function getPaymentTypeCardWithEpayId($_typeid)
    {
        $_type = [
            ['cardname' => 'Dankort/Visa-Dankort', 'id' => '1', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'eDankort', 'id' => '2', 'cardtype' => 'Netbank'],
            ['cardname' => 'Visa / Visa Electron', 'id' => '3', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'MasterCard', 'id' => '4', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'JCB', 'id' => '6', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'Maestro', 'id' => '	7', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'Diners Club', 'id' => '8', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'American Express', 'id' => '9', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'Forbrugsforeningen', 'id' => '11', 'cardtype' => 'Betalingskort'],
            ['cardname' => 'Danske Netbetalinger', 'id' => '13', 'cardtype' => 'Netbank'],
            ['cardname' => 'PayPal', 'id' => '14', 'cardtype' => 'Andet'],
            ['cardname' => 'Klarna', 'id' => '17', 'cardtype' => 'Faktura'],
            ['cardname' => 'SEB (SE)', 'id' => '19', 'cardtype' => 'Netbank'],
            ['cardname' => 'Nordea (SE)', 'id' => '20', 'cardtype' => 'Netbank'],
            ['cardname' => 'Handelsbanken (SE)', 'id' => '	21', 'cardtype' => 'Netbank'],
            ['cardname' => 'Swedbank (SE)', 'id' => '22', 'cardtype' => 'Netbank'],
            ['cardname' => 'ViaBill', 'id' => '	23', 'cardtype' => 'ViaBill'],
            ['cardname' => 'Beeptify', 'id' => '24', 'cardtype' => 'Andet'],
            ['cardname' => 'iDEAL', 'id' => '25', 'cardtype' => 'Netbank'],
            ['cardname' => 'Paii', 'id' => '27', 'cardtype' => 'Mobil'],
            ['cardname' => 'Brandts Gavekort', 'id' => '28', 'cardtype' => 'Gavekort'],
            ['cardname' => 'MobilePay Online', 'id' => '29', 'cardtype' => 'mobil']
        ];


        foreach ($_type as $card) {
            if ($_typeid == $card['id']) {
                return $card['cardname'];
            }
        }

        return 'No card with id: ' .  $_typeid;

    }

    public function saveTransactionData(Event $event)
    {


        $txnFee = craft()->request->getParam('txnfee');
        $paymenttype = craft()->request->getParam('paymenttype');
        $currency = craft()->request->getParam('currency');
        $order = $event->params['order'];
        $orderId = $order->id;

        $params = array();

        foreach ($_GET as $key => $value) {
            $params[$key] = $value;
        }

        $response = json_encode($params);

        craft()->db->createCommand()
            ->insert('elektronikforsikring_orderattributes', array(
                'txnfee' => $txnFee,
                'paymenttype' => $paymenttype,
                'currency' => $currency,
                'orderId' => $orderId,
                'response' => $response,
            ));

    }


}