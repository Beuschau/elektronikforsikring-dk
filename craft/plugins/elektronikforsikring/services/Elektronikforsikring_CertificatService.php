<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_CertificatService extends BaseApplicationComponent
{
    /**
     * This function can literally be anything you want, and you can have as many service functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     craft()->elektronikforsikring_sendOrders->exampleService()
     */


    public function getValueByName($_name)
    {

        $return = "<div class='error'>$_name</div>";
        // Get the settings global set
        $settings = craft()->globals->getSetByHandle('forsikringscertifikat');

        // Access the address field
        $cert = $settings->forsikringscertifikat;
        foreach ($cert as $item) {
            if($item->type == "line")
            {
                if($_name == $item->fieldVariabel)
                {
                    $return =  $item;
                }
            }
        }

        return $return;


    }

}