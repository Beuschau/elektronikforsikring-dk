<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_CardTypeService extends BaseApplicationComponent
{
    /**
     * This function can literally be anything you want, and you can have as many service functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     craft()->elektronikforsikring_sendOrders->exampleService()
     */

    public $paymentType = array(

        [
            'name' => 'Dankort/Visa-Dankort',
            'id' => '1',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'eDankort',
            'id' => '2',
            'type' => 'Netbank',
        ],
        [
            'name' => 'Visa / Visa Electron',
            'id' => '3',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'MasterCard',
            'id' => '4',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'JCB',
            'id' => '6',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Maestro',
            'id' => '7',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Diners Club',
            'id' => '8',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'American Express',
            'id' => '9',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Forbrugsforeningen',
            'id' => '11',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Forbrugsforeningen',
            'id' => '13',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'PayPal',
            'id' => '14',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Klarna',
            'id' => '17',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'SEB (SE)',
            'id' => '19',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Nordea (SE)',
            'id' => '20',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Handelsbanken (SE)',
            'id' => '21',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Swedbank (SE)',
            'id' => '22',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'ViaBill',
            'id' => '23',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Beeptify',
            'id' => '24',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'iDEAL',
            'id' => '25',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Paii',
            'id' => '27',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'Brandts Gavekortt',
            'id' => '28',
            'type' => 'Betalingskort',
        ],
        [
            'name' => 'MobilePay Online',
            'id' => '29',
            'type' => 'Betalingskort',
        ],
    );


    public function getPaymentType($_id)
    {


        foreach ( $this->paymentType as $card)
        {
               if($_id == $card['id'])
               {
                   return $card['name'];
               }
        }

    }

}