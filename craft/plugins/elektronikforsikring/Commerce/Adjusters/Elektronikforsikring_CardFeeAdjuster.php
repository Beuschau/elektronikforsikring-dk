<?php

namespace Commerce\Adjusters;


use Craft\Commerce_LineItemModel;
use Craft\Commerce_OrderModel;
use Craft\Commerce_OrderAdjustmentModel;
use Craft\HttpRequestService;

/**
 * Month Adjustments
 *
 * Class Commerce_ShippingAdjuster
 *
 * @package Commerce\Adjusters
 */
class Elektronikforsikring_CardFeeAdjuster implements Commerce_AdjusterInterface
{

    /**
     * @param Commerce_OrderModel $order
     * @param Commerce_LineItemModel[] $lineItems
     *
     * @return \Craft\Commerce_OrderAdjustmentModel[]
     */
    public function adjust(Commerce_OrderModel &$order, array $lineItems = [])
    {
        $myAdjuster = new Commerce_OrderAdjustmentModel();

        if ($_GET['txnfee']) {

            $order->baseShippingCost = $order->baseShippingCost + $this->getPriceForThisMonth();
            $myAdjuster->type = "CardFee";
            $myAdjuster->name = "Kortgebyr";
            $myAdjuster->description = "Der er et gebyr for betaling med kreditkort";
            $myAdjuster->amount = $_GET['txnfee'] * 100;
            $myAdjuster->orderId = $order->id;
            $myAdjuster->optionsJson = ['lineItemsAffected' => null];
            $myAdjuster->included = false;



        }

        return [$myAdjuster];

    }

    public function getPriceForThisMonth()
    {
        $insurrence = new Elektronikforsikring_InsurrencePriceService();
        return $insurrence->getPriceToday();
    }

}
