<?php

namespace Commerce\Adjusters;


use Craft\Commerce_LineItemModel;
use Craft\Commerce_OrderModel;
use Craft\Elektronikforsikring_InsurrencePriceService;
use Craft\Commerce_OrderAdjustmentModel;

/**
 * Month Adjustments
 *
 * Class Commerce_ShippingAdjuster
 *
 * @package Commerce\Adjusters
 */
class Elektronikforsikring_MonthAdjuster implements Commerce_AdjusterInterface
{

    /**
     * @param Commerce_OrderModel $order
     * @param Commerce_LineItemModel[] $lineItems
     *
     * @return \Craft\Commerce_OrderAdjustmentModel[]
     */
    public function adjust(Commerce_OrderModel &$order, array $lineItems = [])
    {
        $insurrence = new Elektronikforsikring_InsurrencePriceService();
        $myAdjuster = new Commerce_OrderAdjustmentModel();
        $order->baseShippingCost = $order->baseShippingCost + $this->getPriceForThisMonth();
        $today = getDate();
        $myAdjuster->type = "Elektronikforsikring";
        $myAdjuster->name = "Resten af " . strtolower($insurrence->getMonthName($today['mon']));
        $myAdjuster->description = "" . $insurrence->daysLeftInMonth() ." dage";
        $myAdjuster->amount = $this->getPriceForThisMonth();
        $myAdjuster->orderId = $order->id;
        $myAdjuster->optionsJson = ['lineItemsAffected'=>null];
        $myAdjuster->included = false;

        return [$myAdjuster];

    }

    public function getPriceForThisMonth()
    {
        $insurrence = new Elektronikforsikring_InsurrencePriceService();
        return $insurrence->getPriceToday();
    }

}
