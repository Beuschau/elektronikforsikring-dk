<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_SendOrdersController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array('actionIndex','actionResetOrders',
    );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/elektronikforsikring
     */

    public function actionIndex()
    {
        $_orders = $this->_getOrders();

        $_ordersAsString = (string)$this->_createOrderList($_orders);

        $_orderList = mb_convert_encoding($_ordersAsString, "windows-1252");
        $craftEmail = new EmailModel();
        $craftEmail->toEmail = craft()->plugins->getPlugin('elektronikforsikring')->getSettings()->navisionEmail;
        $craftEmail->fromEmail = 'order@18digital.dk';

        //csv filename
        $fileName = craft()->plugins->getPlugin('elektronikforsikring')->getSettings()->csvFileName;


        //server storage path
        $storagePath = craft()->path->getStoragePath();
        $storageFolder = 'submission';

        if (!file_exists($storagePath . $storageFolder)) {
            mkdir($storagePath . $storageFolder, 0777, true);
        }
        $fileNamePath = $storagePath . $storageFolder . '/' . $fileName;
        $uniqueId = time() . '-' . mt_rand();
        $fileNameBackupPath = $storagePath . $storageFolder . '/' . $uniqueId . '-' . $fileName .'.csv';

        //write to server
        $submissionFile = fopen($fileNamePath, "w");
        $submissionBackupFile = fopen($fileNameBackupPath, "w");
        mb_convert_encoding((string)($submissionBackupFile), 'windows-1252', 'utf-8');
        fwrite($submissionFile, $_orderList);
        fwrite($submissionBackupFile, $_orderList);

        fclose($submissionFile);

        $craftEmail->addAttachment($fileNamePath, $fileName, 'base64', 'application/octet-stream');

        craft()->email->sendEmail($craftEmail);

        $this->updateOrderStatus($_orders);
    }

    public function actionResetOrders()
    {

        // update `craft_commerce_orders` set orderStatusId = 2;

        $query = craft()->db->createCommand();
        $query->update('commerce_orders', array(
            'orderStatusId' => 2));
    }

    private function updateOrderStatus($_orders)
    {

        foreach ($_orders as $_order) {

            $this->setOrderStatusToNavistionStatement($_order->id);
        }
    }

    private function setOrderStatusToNavistionStatement($_id)
    {
        $query = craft()->db->createCommand();
        $query->update('commerce_orders', array(
            'orderStatusId' => craft()->plugins->getPlugin('elektronikforsikring')->getSettings()->orderStatusId
        ), 'id=:id', array(':id' => $_id));
    }

    public function countOrders()
    {


        $query = craft()->db->createCommand();
        $query->select('commerce_orders')->count();

        return $query;
    }

    private function _createOrderList($_orders)
    {
        $_return = '';
        $separator = ';';

        $settings = craft()->plugins->getPlugin('elektronikforsikring')->getSettings();
        $forsikring = $settings->forsikring;
        $kladetype = $settings->kladetype;


        foreach ($_orders as $_order) {

            $transactionData = craft()->elektronikforsikring_orderTransaction->getTransactionDataWithOrderId($_order->id);
            $response = json_decode($transactionData[0]['response'], true);

            $txnfee = $transactionData[0]['txnfee'];
            $paymenttype = $transactionData[0]['paymenttype'];
            $cardInfo = craft()->elektronikforsikring_cardType->getPaymentType($paymenttype);
            $txnid = $response['txnid'];

            $_return .= '4' . $separator;
            $_return .= $_order->shippingAddress->firstName . " " . $_order->shippingAddress->lastName . $separator;
            $_return .= $_order->shippingAddress->address1 . $separator;
            $_return .= $_order->shippingAddress->zipCode . $separator;
            $_return .= $_order->shippingAddress->city . $separator;
            $_return .= $_order->shippingAddress->phone . $separator;
            $_return .= $_order->customer->email . $separator;
            $_return .= $_order->id . $separator;
            $_return .= date("d-m-Y",strtotime($_order->dateOrdered)) . $separator;
            $_return .= $forsikring . $separator;
            $_return .= number_format($_order->totalPrice, 2, ',', '') . $separator;
            $_return .= $kladetype . $separator;
            $_return .= $txnid . $separator;
            $_return .= $cardInfo . $separator;
            $_return .= number_format($txnfee / 100, 2, ',', '') . $separator;
            $_return .= number_format($_order->baseShippingCost, 2, ',', '');
            $_return .= "\r\n";

            /**
             * Forhandlernr.
             * Kundenavn
             * Adresse
             * Postnr
             * By
             * Telefon
             * E-mail
             * Købsnota
             * Købsdato
             * Forsikring
             * Rapporteret præmie
             * Kladdetype
             * ePay Subscription ID
             * Card Type
             * Card Fee
             * Rest måned beløb
             **/
        }

        return $_return;
    }

    private function _getOrders()
    {
        $orderStatusId = '2';
        // $limit = '10';

        $criteria = craft()->elements->getCriteria('Commerce_Order');
        $criteria->completed = true;
        $criteria->dateOrdered = "NOT NULL";
        //$criteria->limit = $limit;
        $criteria->order = 'dateOrdered desc';

        if ($orderStatusId) {
            $criteria->orderStatusId = $orderStatusId;
        }

        return $criteria->find();
    }
}