<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_TransactionController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array('actionIndex','actionCertificate', 'actionEmailOrder', 'actionPdf'
    );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/elektronikforsikring
     */

    public function actionTransaction()
    {
        $orders = craft()->elektronikforsikring_orderTransaction->getOrderWithTransactionData('1000015');
        var_dump($orders);

        echo $orders[0]['txnfee'];
    }

    public function actionOrderTransaction()
    {
        $path = craft()->path->getSiteTemplatesPath();
        craft()->path->setTemplatesPath($path);
        $order = $this->_getOrders(1)[0];
        $pluginSettings = craft()->plugins->getPlugin('elektronikforsikring');
        $this->renderTemplate('commerce/_emails/orderConfirmation', ['order' => $order, 'entry' => $pluginSettings]);

    }

    private function _getOrders($_limit = 3)
    {
        $orderStatusId = '2';
        // $limit = '10';

        $criteria = craft()->elements->getCriteria('Commerce_Order');
        $criteria->completed = true;
        $criteria->dateOrdered = "NOT NULL";
        $criteria->limit = $_limit;
        $criteria->order = 'dateOrdered desc';

        if ($orderStatusId) {
            $criteria->orderStatusId = $orderStatusId;
        }

        return $criteria->find();
    }

}
