<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * elektronikforsikring Twig Extension
 *
 * --snip--
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators, global variables, and
 * functions. You can even extend the parser itself with node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class ElektronikforsikringTwigExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'Elektronikforsikring';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'currency' => new \Twig_Filter_Method($this, 'currency'),
        );
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'currency' => new \Twig_Function_Method($this, 'currency'),
        );
    }



    public function currency($num = null)
    {
        $result = number_format($num,2,',','.')." kr.";

        return $result;
    }
}