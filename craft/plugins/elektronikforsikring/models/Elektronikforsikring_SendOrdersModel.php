<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Elektronikforsikring_SendOrders Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

namespace Craft;

class Elektronikforsikring_SendOrdersModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'navisionEmail'     => array(AttributeType::String, 'default' => ''),
            'forsikring'     => array(AttributeType::String, 'default' => ''),
            'kladdetype'     => array(AttributeType::String, 'default' => ''),
            'csvFileName'     => array(AttributeType::String, 'default' => ''),
            'dropdown'     => array(AttributeType::String, 'default' => ''),
        ));
    }

}