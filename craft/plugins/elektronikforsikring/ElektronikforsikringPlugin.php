<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * Variuos settings for order and sales activaties in commerce
 *
 * --snip--
 * Craft plugins are very much like little applications in and of themselves. We’ve made it as simple as we can,
 * but the training wheels are off. A little prior knowledge is going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL, as well as some semi-
 * advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 * --snip--
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */


namespace Craft;
require_once('Commerce/Adjusters/Elektronikforsikring_MonthAdjuster.php');

use Commerce\Adjusters\Elektronikforsikring_MonthAdjuster;

class ElektronikforsikringPlugin extends BasePlugin
{
    /**
     * Called after the plugin class is instantiated; do any one-time initialization here such as hooks and events:
     *
     * craft()->on('entries.saveEntry', function(Event $event) {
     *    // ...
     * });
     *
     * or loading any third party Composer packages via:
     *
     * require_once __DIR__ . '/vendor/autoload.php';
     *
     * @return mixed
     */
    public function init()
    {
        $this->initEventHandlers();

    }

    public function registerSiteRoutes()
    {
        return array(

            'email/customers/order(?<id>)' => array('action' => 'elektronikforsikring/test/emailOrder'),
            'email/customers/order/(?P<orderid>[^\/]+)' => array('action' => 'elektronikforsikring/test/emailOrder'),

        );
    }

    public function defineAdditionalOrderTableAttributes()
    {
        return array(
            'foo' => "Foo",
            'bar' => "Bar",
        );
    }



    public function initEventHandlers()
    {

        craft()->on('commerce_orders.onBeforeOrderComplete', array(craft()->elektronikforsikring_orderTransaction, 'saveTransactionData'));
        /*craft()->on('commerce_addresses.onBeforeSaveAddress', function (Event $event) {
            $address = $event->params['address'];



            if (empty($address->lastName)) {
                $address->addError('lastName', Craft::t('Du skal udfylde dit efternavn'));
                $event->performAction = false;
            }

            if (empty($address->city)) {
                $address->addError('city', Craft::t('Du skal udfylde by'));
                $event->performAction = false;
            }

            if (empty($address->zipCode)) {
                $address->addError('zipCode', Craft::t('Du skal udfylde postnr.'));
                $event->performAction = false;
            }
            if (empty($address->phone)) {
                $address->addError('phone', Craft::t('Du skal udfylde dit telefonnr.'));
                $event->performAction = false;
            }
            if (empty($address->address1)) {
                $address->addError('address1', Craft::t('Du skal udfylde din adresse'));

            }



        });*/



    }

    public function getValueByName($_name)
    {
        return craft()->elektronikforsikring_certificat->getValueByName($_name);
    }

    public function commerce_registerOrderAdjusters()
    {

        return [
            new Elektronikforsikring_MonthAdjuster
        ];
    }

    public function getOrderTableAttributeHtml(Commerce_OrderModel $order, $attribute)
    {
        if ($attribute == 'totalPrice')
        {
            return $order->totalPaid * 100;
        }
    }


    /**
     * Returns the user-facing name.
     *
     * @return mixed
     */
    public function getName()
    {
        return 'Elektronikforsikring';
    }

    /**
     * Plugins can have descriptions of themselves displayed on the Plugins page by adding a getDescription() method
     * on the primary plugin class:
     *
     * @return mixed
     */
    public function getDescription()
    {
        return Craft::t('Variuos settings for order and sales activaties in commerce ');
    }

    /**
     * Plugins can have links to their documentation on the Plugins page by adding a getDocumentationUrl() method on
     * the primary plugin class:
     *
     * @return string
     */
    public function getDocumentationUrl()
    {
        return '???';
    }

    /**
     * Plugins can now take part in Craft’s update notifications, and display release notes on the Updates page, by
     * providing a JSON feed that describes new releases, and adding a getReleaseFeedUrl() method on the primary
     * plugin class.
     *
     * @return string
     */
    public function getReleaseFeedUrl()
    {
        return '???';
    }

    /**
     * Returns the version number.
     *
     * @return string
     */
    public function getVersion()
    {
        return '1.0.0';
    }

    /**
     * As of Craft 2.5, Craft no longer takes the whole site down every time a plugin’s version number changes, in
     * case there are any new migrations that need to be run. Instead plugins must explicitly tell Craft that they
     * have new migrations by returning a new (higher) schema version number with a getSchemaVersion() method on
     * their primary plugin class:
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return '1.0.0';
    }

    /**
     * Returns the developer’s name.
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Martin Beuschau';
    }

    /**
     * Returns the developer’s website URL.
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return '18digital.dk';
    }

    /**
     * Returns whether the plugin should get its own tab in the CP header.
     *
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     * Add any Twig extensions.
     *
     * @return mixed
     */
    public function addTwigExtension()
    {
        Craft::import('plugins.elektronikforsikring.twigextensions.ElektronikforsikringTwigExtension');

        return new ElektronikforsikringTwigExtension();
    }

    /**
     * Called right before your plugin’s row gets stored in the plugins database table, and tables have been created
     * for it based on its records.
     */
    public function onBeforeInstall()
    {
    }

    /**
     * Called right after your plugin’s row has been stored in the plugins database table, and tables have been
     * created for it based on its records.
     */
    public function onAfterInstall()
    {
    }

    /**
     * Called right before your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onBeforeUninstall()
    {
    }

    /**
     * Called right after your plugin’s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onAfterUninstall()
    {
    }

    /**
     * Defines the attributes that model your plugin’s available settings.
     *
     * @return array
     */
    protected function defineSettings()
    {
        return array(
            'navisionEmail' => array(AttributeType::String, 'label' => '', 'default' => ''),
            'forsikring' => array(AttributeType::String, 'label' => '', 'default' => ''),
            'kladdetype' => array(AttributeType::Number, 'label' => '', 'default' => ''),
            'csvFileName' => array(AttributeType::String, 'label' => '', 'default' => ''),
            'kladetype' => array(AttributeType::Number, 'label' => '', 'default' => ''),
            'orderStatusId' => array(AttributeType::Number, 'label' => '', 'default' => ''),
        );
    }

    /**
     * Returns the HTML that displays your plugin’s settings.
     *
     * @return mixed
     */


    public function getSettingsHtml()
    {
        $orderStatus = craft()->commerce_orderStatuses->getAllOrderStatuses();
        $orderStatuses = array();

        foreach ($orderStatus as $key) {
            $orderStatuses[] = ['label' => $key['name'], 'value' => $key['id']];
        }

        return craft()->templates->render('elektronikforsikring/Elektronikforsikring_Settings', array(
            'settings' => $this->getSettings(),
            'orderStatuses' => $orderStatuses,
        ));
    }

    /**
     * If you need to do any processing on your settings’ post data before they’re saved to the database, you can
     * do it with the prepSettings() method:
     *
     * @param mixed $settings The Widget's settings
     *
     * @return mixed
     */
    public function prepSettings($settings)
    {
        // Modify $settings here...

        return $settings;
    }


}
