<?php
/**
 * elektronikforsikring plugin for Craft CMS
 *
 * elektronikforsikring Translation
 *
 * @author    Martin Beuschau
 * @copyright Copyright (c) 2016 Martin Beuschau
 * @link      18digital.dk
 * @package   Elektronikforsikring
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
