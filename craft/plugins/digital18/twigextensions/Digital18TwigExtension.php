<?php
/**
 * Martin Beuschau
 */
namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;


class Digital18TwigExtension extends \Twig_Extension
{
    private $digitToWord = array(
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
    );

    public function getName()
    {
        return '18digital';
    }

    public function getFilters()
    {
        return array(
            'wordItUp' => new Twig_Filter_Method($this, 'wordItUp'),
            'recurrent' => new Twig_Filter_Method($this, 'recurrent'),
            'creditCardWithDraw' => new Twig_Filter_Method($this, 'creditCardWithDraw'),
            'getPriceForThisMonth' => new Twig_Filter_Method($this, 'getPriceForThisMonth'),
            'shortCodes' => new Twig_Filter_Method($this, 'shortCodes'),
            'cartMonth' => new Twig_Filter_Method($this, 'cartMonth'),
            'md5' => new Twig_Filter_Method($this, 'md5'),
            'ePay' => new Twig_Filter_Method($this, 'ePay'),
        );
    }

    public function wordItUp($number)
    {
        $output = array();

        foreach (str_split(intval($number)) as $digit) {
            $output[] = $this->digitToWord[$digit];
        }

        return implode(' ', $output);
    }

    public function recurrent($number)
    {

        return $number . "./md";
    }

    public function cartMonth($_cart)
    {

        return $_cart;
    }

    public function md5($_md5)
    {

        return md5($_md5);
    }

    private function _daysLeftInMonth()
    {
        $date = new DateTime('now');
        $nowTimestamp = $date->getTimestamp();
        $date->modify('first day of next month');
        $firstDayOfNextMonthTimestamp = $date->getTimestamp();
        return ($firstDayOfNextMonthTimestamp - $nowTimestamp) / 86400;
    }

    public function creditCardWithDraw($number)
    {
        $_todayNumber = date('d');
        if ($_todayNumber == 1) {
            return $number;
        }
        return $number + ($this->_daysLeftInMonth() * ($number / 30)) | round(0, 1);
    }

    public function getPriceForThisMonth($_price)
    {
        return $this->_daysLeftInMonth() * ($_price / 30) | round(0, 1);
    }


    public function shortCodes($attributes, $price)
    {
        $string = $attributes;
        $search = "/\[([^\]]+)\]/";
        $replace = $this->getPriceForThisMonth($price) . " kr.";
        $result = preg_replace($search, $replace, $string);

        echo $result;

    }


}
