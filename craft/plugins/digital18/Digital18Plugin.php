<?php
namespace Craft;

use Craft\Commerce\Adjusters;

class Digital18Plugin extends BasePlugin
{
    public function getName()
    {
        return Craft::t('18Digital');
    }

    public function getVersion()
    {
        return '1.0.0';
    }

    public function commerce_registerOrderAdjusters()
    {

        return [
            //new \Craft\PluginCustomClass(')
        ];
    }

    public function getDescription()
    {
        return 'Various amazing filters and general help with the current website';
    }

    public function getDeveloper()
    {
        return 'Martin Beuschau';
    }

    public function getDeveloperUrl()
    {
        return 'http://18digital.dk';
    }

    public function hasCpSection()
    {
        return false;
    }

    public function addTwigExtension()
    {
        Craft::import('plugins.digital18.twigextensions.Digital18TwigExtension');
        return new Digital18TwigExtension();
    }

}